//
//  NSData+Extension.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 01/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

extension NSData{
    func getJsonFromNSData() throws -> NSDictionary? {
        if let json = try NSJSONSerialization.JSONObjectWithData(self, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary{
            return json
        }else{
            return nil
        }
    }
}

