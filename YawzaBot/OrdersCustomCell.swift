//
//  OrdersCustomCell.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 06/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class OrdersCustomCell: UITableViewCell {
    @IBOutlet var type: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var total: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
