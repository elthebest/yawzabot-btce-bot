//
//  WarningDialog.swift
//  grandbazaar
//
//  Created by Maxim Shmotin on 29/01/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

public class Dialog {
    enum Title: String{
        case SUCCESS = "Success"
        case WARNING = "Warning"
        case ERROR = "Error"
    }
    
    class func show(title: Title, message: String){
        let ndialog = UIAlertView(title: NSLocalizedString(title.rawValue, comment: "internet"), message: message, delegate: nil,
            cancelButtonTitle: NSLocalizedString("OK", comment: "cancel"))
        ndialog.alertViewStyle = UIAlertViewStyle.Default
        ndialog.show()
    }
}
