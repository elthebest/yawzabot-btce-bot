//
//  UIResponder+FirstResponder.swift
//  davinci
//
//  Created by Mihail Shulepov on 05/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//
import Foundation

private var _currentFirstResponder: UIResponder?

public extension UIResponder {
    public class func currentFirstResponder() -> UIResponder? {
        _currentFirstResponder = nil
        UIApplication.sharedApplication().sendAction("findFirstResponder:", to: nil, from: nil, forEvent: nil)
        return _currentFirstResponder
    }
    
    public func findFirstResponder(sender: AnyObject!) {
        _currentFirstResponder = self
    }
}