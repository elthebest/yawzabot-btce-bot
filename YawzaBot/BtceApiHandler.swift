//
//  BtceApiHandler.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 12/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

public class BtceApiHandler: NSObject {
    
    private enum Config: String {
        case BTCE_TICKER_URL = "https://btc-e.com/api/3/ticker/"
        case BTCE_TAPI_URL = "https://btc-e.com/tapi"
        case NONCE = "Nonce"
        case METHOD_TRADE = "Trade"
        case METHOD_GET_INFO = "getInfo"
        case METHOD_ACTIVE_ORDERS = "ActiveOrders"
        case METHOD_CANCEL_ORDER = "CancelOrder"
        case METHOD_TRADE_HISTORY = "TradeHistory"
        case METHOD_WITHDRAW_COIN = "WithdrawCoin"
    }

    public let dispatch_serial_queue: dispatch_queue_t = dispatch_queue_create("com.planemo.yawzaBot", nil)
    private var api_key: String!
    private var secret_key: String!
    
    class var sharedInstanse : BtceApiHandler {
        struct Static {
            static var instance: BtceApiHandler?
            static var token: dispatch_once_t = 0
        }
        dispatch_once(&Static.token){
            Static.instance = BtceApiHandler()
        }
        return Static.instance!
    }
    
    private var nonce: Int{
        get{
            let value = NSUserDefaults.standardUserDefaults().integerForKey(Config.NONCE.rawValue)
            return (value == 0) ? calculateInitialNonce(): value
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: Config.NONCE.rawValue)
        }
    }
    
    override init() {
        super.init()
        setupInitValues()
    }
    
    internal func setupInitValues(){
        if let apiKey = Authentication.apiKey, secretKey = Authentication.secretKey {
                self.api_key = apiKey
                self.secret_key = secretKey
        }else{
            ifConfigExist()
        }
    }
    
    private func ifConfigExist(){
        var mySettings: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Config", ofType: ".plist"){
            mySettings = NSDictionary(contentsOfFile: path)!
            self.api_key = mySettings?.objectForKey("api_key") as! String
            self.secret_key = mySettings?.objectForKey("secret_key") as! String
        }else{
            print("Config file not found")
        }
    }
    
    public func isConfigDataUsed() -> Bool{
        var mySettings: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Config", ofType: ".plist"){
            mySettings = NSDictionary(contentsOfFile: path)!
            return (mySettings?.objectForKey("api_key") as! String == self.api_key) &&
             (mySettings?.objectForKey("secret_key") as! String == self.secret_key)
        }
        return false
    }
    
    ///Get response from server for POST
    internal func buySellCurrency(tradePair: String, rate: String, amount: String, action: Order.OrderType, completionHandler: Bool -> Void){
        NSLog("start buySellBitcoin")
        let post = NSMutableDictionary()
        post.setObject(Config.METHOD_TRADE.rawValue, forKey: "method")
        post.setObject(tradePair, forKey: "pair")
        post.setObject(action.rawValue, forKey: "type")
        post.setObject(rate, forKey: "rate")
        post.setObject(amount, forKey: "amount")
        dispatch_async(dispatch_serial_queue){
            if let json = self.getResponseFromServerForPost(post)?.getJsonFromNSData(){
                if (json["success"] as? Int == 0){
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
            }else{
                completionHandler(false)
            }
        }
    }

    ///Get response from server for POST
    public func cancelActiveOrder(id: Int){
        NSLog("start cancelActiveOrder")
        let post = NSMutableDictionary()
        post.setObject(Config.METHOD_CANCEL_ORDER.rawValue, forKey: "method")
        post.setObject(id, forKey: "order_id")
        dispatch_async(self.dispatch_serial_queue){
            let _ = self.getResponseFromServerForPost(post)
        }
    }
    
    ///Get response from server for POST
    public func getActiveOrders(tradePair: String) -> NSDictionary? {
        let post = NSMutableDictionary()
        post.setObject(Config.METHOD_ACTIVE_ORDERS.rawValue, forKey: "method")
        post.setObject(tradePair, forKey: "pair")
        if let data = self.getResponseFromServerForPost(post){
            return data.getJsonFromNSData()
        }
        return NSDictionary()
    }
    
    ///Get response from server for POST
    public func updateAccountInfo() -> NSDictionary?{
        NSLog("start updateAccountInfo")
        let post = NSMutableDictionary()
        post.setObject(Config.METHOD_GET_INFO.rawValue, forKey: "method")
        if let data = self.getResponseFromServerForPost(post){
            return data.getJsonFromNSData()
        }
        return nil
    }
    
    ///Get response from server for POST
    public func getTradeHistory(tradePair: String) -> NSDictionary?{
        NSLog("start getTradeHistory for \(tradePair)")
        let post = NSMutableDictionary()
        post.setObject(Config.METHOD_TRADE_HISTORY.rawValue, forKey: "method")
        post.setObject(tradePair, forKey: "pair")
        post.setObject("DESC", forKey: "order")
        if let data = self.getResponseFromServerForPost(post){
            return data.getJsonFromNSData()
        }
        return nil
    }
    
    ///Get response from server for POST
    public func withdrawCoin(currency: String, amount: String, address: String) -> NSDictionary?{
        NSLog("start withdrawCoins for \(currency)")
        let post = NSMutableDictionary()
        post.setObject(Config.METHOD_WITHDRAW_COIN.rawValue, forKey: "method")
        post.setObject(currency, forKey: "coinName")
        post.setObject(amount, forKey: "amount")
        post.setObject(address, forKey: "address")
        if let data = self.getResponseFromServerForPost(post){
            return data.getJsonFromNSData()
        }
        return nil
    }
    
    ///Get response from server for URL
    public func getTradePairRate(tpCode: String) -> NSDictionary?{
        if let data = self.getResponseFromPublicServerUrl("\(Config.BTCE_TICKER_URL.rawValue)\(tpCode)?ignore_invalid=1"){
            return data.getJsonFromNSData()
        }
        return nil
    }
    
    private func getResponseFromServerForPost(postDictionary: NSDictionary) -> NSData?{
        var post: String!
        var i: Int = 0
        for (key, value) in postDictionary {
            if (i==0){
                post = "\(key)=\(value)"
            }else{
                post = "\(post)&\(key)=\(value)"
            }
            i++;
        }
        post = "\(post)&nonce=\(nonce)"
        nonce++
        print(post)
        let signedPost = hmacForKeyAndData(secret_key, data: post) as String
        let request = NSMutableURLRequest(URL: NSURL(string: Config.BTCE_TAPI_URL.rawValue as String)!)
        request.HTTPMethod = "POST"
        request.setValue(api_key, forHTTPHeaderField: "key")
        request.setValue(signedPost, forHTTPHeaderField: "sign")
        
        let requestBodyData = (post as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        request.HTTPBody = requestBodyData
        
        var error: NSError?
        let theResponse: AutoreleasingUnsafeMutablePointer <NSURLResponse?>=nil
        let responseData = try! NSURLConnection.sendSynchronousRequest(request, returningResponse: theResponse) as NSData!
        if (error != nil){
            return nil
        }
        if let json = responseData.getJsonFromNSData() where (json["success"] as? Int) == 0 {
            if let error = json["error"] as? String where error.lowercaseString.rangeOfString("send:") != nil{
                let range = error.lowercaseString.rangeOfString("send:")!
                let firstLetter = error.startIndex.distanceTo(range.endIndex)
                let newRange = Range(start: firstLetter, end: error.length)
                nonce = Int(error[newRange])!
                return nil
            }
        }
        return responseData
    }
    
    private func calculateInitialNonce()->Int{
        let dataFormat = NSDateFormatter()
        dataFormat.dateFormat = "yyyy-MM-dd HH:mm:ss xxxx"
        let timeStamp = NSDate().timeIntervalSinceDate(dataFormat.dateFromString("2012-04-18 00:00:03 +0600")!)
        let currentNonce = NSNumber(double: timeStamp) as Int
        return currentNonce
    }
    
    public func getResponseFromPublicServerUrl(urlString: NSString) -> NSData? {
        let url = NSURL(string: urlString as String)
        let request = NSURLRequest(URL: url!)
        let theResponse: AutoreleasingUnsafeMutablePointer <NSURLResponse?>=nil

        var error: NSError?
        let responseData = try! NSURLConnection.sendSynchronousRequest(request, returningResponse: theResponse) as NSData!
        if (error != nil){
            return nil
        }
        if let json = responseData.getJsonFromNSData() {
            if ((json["success"] as? Int) == 0){
                return nil
            }
        }
        return responseData
    }
    
    private func hmacForKeyAndData(key: NSString, data: NSString)->NSString{
        let cKey =  key.cStringUsingEncoding(NSASCIIStringEncoding)
        let cData = data.cStringUsingEncoding(NSASCIIStringEncoding)
        let _ = [CUnsignedChar](count: Int(CC_SHA512_DIGEST_LENGTH), repeatedValue: 0)
        let digestLen = Int(CC_SHA512_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
        print("CCHmac")
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA512), cKey, Int(key.length), cData, Int(data.length), result)
        let hashString =  NSMutableString(capacity: Int(CC_SHA512_DIGEST_LENGTH))
        for i in 0..<digestLen{
            hashString.appendFormat("%02x", result[i])
        }
        return hashString
    }
}
