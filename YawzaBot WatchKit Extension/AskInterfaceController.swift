//
//  AskInterfaceController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 01/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import WatchKit
import Foundation
import BtceApiKit

class AskInterfaceController: WKInterfaceController {
    @IBOutlet weak var asksTable: WKInterfaceTable!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }
    
    private func refreshOrderBook(){
        ApiHandler.getOrderBook{ asks, bids in
            if let asksArray = asks{
                self.reloadTable(asksArray)
            }
        }
    }
    
    override func willActivate() {
        super.willActivate()
        refreshOrderBook()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    func reloadTable(array: NSArray) {
        asksTable.setNumberOfRows(array.count, withRowType: "OrderBookTableRow")
        
        for (index, element) in array.enumerate() {
            if let row = asksTable.rowControllerAtIndex(index) as? OrderBookTableRow {
                row.priceLabel.setText("\((element[0] as! Double).format())")
                row.btcLabel.setText("\((element[1] as! Double).format())")
            }
        }
    }
    
    @IBAction func update(){
        refreshOrderBook()
    }

}

extension Double {
    func format() -> String {
        let decimalPlaces: String = ".3"
        return NSString(format: "%\(decimalPlaces)f", self) as String
    }
}
